﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class CalculoCables : MonoBehaviour
{
    public string awg;

    public string Respuesta;

    public GameObject inputFieldawg;

    public GameObject textDisplay;


    public GameObject panelFlujo;
    public GameObject panelFlujoSiNo;
    public bool entro = false;

    public LogicaPersonaje1 j;
    public int P1;
    public LogicaPersonaje1 jugador;


    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        j = FindObjectOfType<LogicaPersonaje1>();

        P1 = j.p1;
    }



    public void OnTriggerEnter(Collider other)
    {
        panelFlujoSiNo.SetActive(true);
        entro = true;
    }
    public void OnTriggerExit(Collider other)
    {
        panelFlujoSiNo.SetActive(false);
        entro = false;
    }








    public void calculo()
    {
        awg = inputFieldawg.GetComponent<Text>().text;

        if (awg=="4/0") { 
      

        textDisplay.GetComponent<Text>().text = " 107.2 ";
    }
        if (awg == "3/0")
        {


            textDisplay.GetComponent<Text>().text = " 85 ";
        }
        if (awg == "2/0")
        {


            textDisplay.GetComponent<Text>().text = " 67.4 ";
        }
        if (awg == "1/0")
        {


            textDisplay.GetComponent<Text>().text = " 53.5 ";
        }
        if (awg == "1")
        {


            textDisplay.GetComponent<Text>().text = " 42.4 ";
        }
        if (awg == "2")
        {


            textDisplay.GetComponent<Text>().text = " 33.6 ";
        }
        if (awg == "3")
        {


            textDisplay.GetComponent<Text>().text = " 26.7 ";
        }
        if (awg == "4")
        {


            textDisplay.GetComponent<Text>().text = " 21.2 ";
        }
        if (awg == "5")
        {


            textDisplay.GetComponent<Text>().text = " 16.7 ";
        }
        if (awg == "6")
        {


            textDisplay.GetComponent<Text>().text = " 13.3 ";
        }
        if (awg == "7")
        {


            textDisplay.GetComponent<Text>().text = " 10.5 ";
        }
        if (awg == "8")
        {
            textDisplay.GetComponent<Text>().text = " 8.3 ";
        }
        if (awg == "9")
        {
            textDisplay.GetComponent<Text>().text = " 6.6 ";
        }
        if (awg == "10")
        {
            textDisplay.GetComponent<Text>().text = " 5.3 ";
        }
        if (awg == "11")
        {
            textDisplay.GetComponent<Text>().text = " 4.2 ";
        }
        if (awg == "12")
        {
            textDisplay.GetComponent<Text>().text = " 3.3 ";
        }
        if (awg == "13")
        {
            textDisplay.GetComponent<Text>().text = " 2.6 ";
        }
        if (awg == "14")
        {
            textDisplay.GetComponent<Text>().text = " 2.1 ";
        }
        if (awg == "15")
        {
            textDisplay.GetComponent<Text>().text = " 1.7 ";
        }
        if (awg == "16")
        {
            textDisplay.GetComponent<Text>().text = " 1.3 ";
        }
        if (awg == "17")
        {
            textDisplay.GetComponent<Text>().text = " 1.03 ";
        }
        if (awg == "18")
        {
            textDisplay.GetComponent<Text>().text = " 0.823 ";
        }
        if (awg == "19")
        {
            textDisplay.GetComponent<Text>().text = " 0.652 ";
        }
        if (awg == "20")
        {
            textDisplay.GetComponent<Text>().text = " 0.517 ";
        }




    }




    public void ButtonSalirGeneralCatalogo()
    {
        if (entro == true)
        {
            panelFlujo.SetActive(false);
            jugador.enabled = true;
        }
    }


    public void No()
    {
        if (entro == true)
        {

            panelFlujo.SetActive(false);

        }
    }


    public void Si()
    {
        if (entro == true)
        {
            panelFlujo.SetActive(true);
            panelFlujoSiNo.SetActive(false);
        }
    }

}
