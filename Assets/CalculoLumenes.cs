﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
public class CalculoLumenes : MonoBehaviour
{

    public string E;
    public string S;
    public string d;
    public string u;
    public string l;

    public string Respuesta;
    public string Respuesta2;
    public string Respuesta3;

    public GameObject inputFieldE;
    public GameObject inputFieldS;
    public GameObject inputFieldd;
    public GameObject inputFieldu;
    public GameObject inputFieldl;
    public GameObject panelFlujo;
    public GameObject panelFlujoSiNo;

    public GameObject textDisplay;
    public GameObject textDisplay2;
    public GameObject textDisplay3;
    public GameObject textDisplayFlujoReuqerido;
    public GameObject textDisplayNumeroLuminarias;

    public bool entro = false;

    public LogicaPersonaje1 j;
    public int P1;
    public LogicaPersonaje1 jugador;

    // Start is called before the first frame update
    void Start()
    {
       
    }

    // Update is called once per frame
    void Update()
    {

        j = FindObjectOfType<LogicaPersonaje1>();

        P1 = j.p1;
    }

    public void OnTriggerEnter(Collider other)
    {
        panelFlujoSiNo.SetActive(true);
        entro = true;
    }
    public void OnTriggerExit(Collider other)
    {
        panelFlujoSiNo.SetActive(false);
        entro = false;
    }







    public void Calculo()
    {
        if (entro == true) { 
        E = inputFieldE.GetComponent<Text>().text;
        S = inputFieldS.GetComponent<Text>().text;

        Respuesta = (double.Parse(E)* double.Parse(S)).ToString();

        textDisplay.GetComponent<Text>().text = Respuesta+ " lum";
        }
    }
    public void FlujoRequerido()
    {
        if (entro == true)
        {
            textDisplayFlujoReuqerido.GetComponent<Text>().text = Respuesta + " lum";
            d = inputFieldd.GetComponent<Text>().text;
            u = inputFieldu.GetComponent<Text>().text;

            Respuesta2 = ((double.Parse(Respuesta) * double.Parse(d)) / double.Parse(u)).ToString();

            textDisplay2.GetComponent<Text>().text = Respuesta2 + " lum";
        }
    }
    public void NumeroLuminarias()
    {
        if (entro == true)
        {
            textDisplayNumeroLuminarias.GetComponent<Text>().text = Respuesta2 + " lum";
            l = inputFieldl.GetComponent<Text>().text;
            Respuesta3 = (double.Parse(Respuesta2) / double.Parse(l)).ToString();
            textDisplay3.GetComponent<Text>().text = Respuesta3 + " lum";
        }
    }

    public void Borrar()
    {
        

        textDisplay.GetComponent<Text>().text = " ";
        textDisplayFlujoReuqerido.GetComponent<Text>().text = " ";
        textDisplay2.GetComponent<Text>().text = " ";
        textDisplay3.GetComponent<Text>().text = " ";

    }
    public void ButtonSalirGeneralCatalogo()
    {
        if (entro == true)
        {
            panelFlujo.SetActive(false);
            jugador.enabled = true;
        }
    }


    public void No()
    {
        if (entro == true)
        {

            panelFlujo.SetActive(false);

        }
    }


    public void Si()
    {
        if (entro == true)
        {
            panelFlujo.SetActive(true);
            panelFlujoSiNo.SetActive(false);
        }
    }


}
