﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Profesores : MonoBehaviour
{
    
    
    public GameObject panelX;
    public GameObject panelQuieresAprenderCircuitos;
    public GameObject panelCircuitos;

    public GameObject panelInfoQEsUnCircuitoElectrico;
    public GameObject panelInfoPartesCircuitoE;
    public GameObject panelInfoTiposCircuitoE;
    public GameObject panelCatalogo;
    public LogicaPersonaje1 jugador;
   
    public LogicaPersonaje1 j;
    public int P1;
    public int hora;
    // Start is called before the first frame update
    void Start()
    {
        jugador = GameObject.FindGameObjectWithTag("Player").GetComponent<LogicaPersonaje1>();
       
    }

    // Update is called once per frame
    void Update()
    {
        hora = (int)EnviroSky.instance.internalHour;
        j = FindObjectOfType<LogicaPersonaje1>();
      
        P1 = j.p1;

        if (P1 == 2) { 
            if (Input.GetKeyDown(KeyCode.X))
        {
                Vector3 posicionJugador = new Vector3(transform.position.x, jugador.gameObject.transform.position.y, transform.position.z);
                jugador.gameObject.transform.LookAt(posicionJugador);
                //----------------------
                //cambio de private a public
                // private Animator anim;
                jugador.anim.SetFloat("VelX", 0);
                jugador.anim.SetFloat("VelY", 0);
              
                jugador.enabled = false;
                panelX.SetActive(false);
                panelQuieresAprenderCircuitos.SetActive(true);
            }
            }
    }
    public void OnTriggerEnter(Collider other)
    {
        if (P1==2)
        {
        panelX.SetActive(true);

        }
    }
    public void OnTriggerExit(Collider other)
    {
        if (P1==2) { 
            panelX.SetActive(false);
        }
    }



    public void No()
    {
        
            jugador.enabled = true;
            panelQuieresAprenderCircuitos.SetActive(false);
            panelX.SetActive(true);
        }
        

    public void Si()
    {    
            panelQuieresAprenderCircuitos.SetActive(false);
            panelCircuitos.SetActive(true);
            jugador.enabled = false;
        
    }
    public void ButtonQEsUnCircuitoE()
    {
        
            panelInfoQEsUnCircuitoElectrico.SetActive(true);
            panelCircuitos.SetActive(false);
            jugador.enabled = false;
        
    }


    public void ButtonPartesCircuitoE()
    {

        panelInfoPartesCircuitoE.SetActive(true);
        panelCircuitos.SetActive(false);
        jugador.enabled = false;

    }


    public void ButtonSalirPartesCircuitoE()
    {

        panelInfoPartesCircuitoE.SetActive(false);
        panelCircuitos.SetActive(true);
        jugador.enabled = false;

    }
    //---------------------------------------------------------
    public void ButtonTiposCircuitoE()
    {

        panelInfoTiposCircuitoE.SetActive(true);
        panelCircuitos.SetActive(false);
        jugador.enabled = false;

    }


    public void ButtonSalirTiposCircuitoE()
    {

        panelInfoTiposCircuitoE.SetActive(false);
        panelCircuitos.SetActive(true);
        jugador.enabled = false;

    }





    //----------------------------------------------------------------
    public void ButtonSalirQEsUnCircuitoE()
    {
        
            panelInfoQEsUnCircuitoElectrico.SetActive(false);
            panelCircuitos.SetActive(true);
            jugador.enabled = false;
        
    }
    public void ButtonSalirGeneral()
    {
        
            panelInfoQEsUnCircuitoElectrico.SetActive(false);
            panelCircuitos.SetActive(false);
            jugador.enabled = true;
        
    }

   



}
