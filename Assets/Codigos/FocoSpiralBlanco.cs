﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FocoSpiralBlanco : MonoBehaviour
{

    public LogicaPersonaje1 j;
    public int P1;
   
    public bool entro;
    public GameObject panelCatalogo;
    public GameObject panelCatalogoSiNo;
    public LogicaPersonaje1 jugador;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        j = FindObjectOfType<LogicaPersonaje1>();

        P1 = j.p1;
        Vector3 posicionJugador = new Vector3(transform.position.x, jugador.gameObject.transform.position.y, transform.position.z);
        jugador.gameObject.transform.LookAt(posicionJugador);
        //----------------------
        //cambio de private a public
        // private Animator anim;
        jugador.anim.SetFloat("VelX", 0);
        jugador.anim.SetFloat("VelY", 0);
    }

    public void OnTriggerEnter(Collider other)
    {
        panelCatalogoSiNo.SetActive(true);
        entro = true;
    }
    public void OnTriggerExit(Collider other)
    {
        panelCatalogoSiNo.SetActive(false);
        entro = false;
    }


    public void No()
    {
        if (entro == true)
        {

            panelCatalogo.SetActive(false);
        }
    }


    public void Si()
    {
        if (entro == true)
        {
            panelCatalogoSiNo.SetActive(false);
            panelCatalogo.SetActive(true);
            jugador.enabled = false;

        }
    }

    public void ButtonSalirGeneralCatalogo()
    {

        panelCatalogo.SetActive(false);
        jugador.enabled = true;

    }


}
