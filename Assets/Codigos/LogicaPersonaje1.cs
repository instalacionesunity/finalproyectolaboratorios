﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LogicaPersonaje1 : MonoBehaviour
{
    
    //caer rapido
    public int fuerzaExtra = 0;
    //
    public float velocidadMovimiento = 0.5f;
    public float velocidadRotacion = 200.0f;

    public Animator anim;
    public float x, y;



    public Rigidbody rb;
    public float fuerzaDeSalto = 8f;
    public bool puedoSaltar;

    public bool Profesora1;
    public bool Profesora2;
    public bool SwitchSimple;
    public bool BombillaLed;
    public bool LuminariaFluorescente4;
    public bool SwitchLuminariaFluorescente4;
    public bool LamparaCuadrada4;
    public bool SwitchLamparaCuadrada4;

    public int p1;
    internal static object instance;
    public GameObject panelCambioCamara;
    public GameObject Foco1;
    public GameObject Foco2;
    public GameObject Foco3;
    public GameObject Foco4;
    public GameObject Foco5;
    public GameObject Foco6;
    public GameObject Foco7;
    public GameObject Foco8;
    public int hora;

    // Start is called before the first frame update
    void Start()
    {

      //esperar 3 secundos
        StartCoroutine("Esperar");
        //----------------------
        Profesora1 = false;
        Profesora2 = false;
        SwitchSimple = false;
        puedoSaltar = false;
        p1 = 0;
        anim = GetComponent<Animator>();

    }

    void FixedUpdate()
    {
        transform.Rotate(0, x * Time.deltaTime * velocidadRotacion, 0);
        transform.Translate(0, 0,y * Time.deltaTime * velocidadMovimiento);
        hora = (int)EnviroSky.instance.internalHour;

       
        if (hora >= 0 && hora <= 6 || hora >= 18 && hora <= 23)
        {
            Foco1.SetActive(true);
            Foco2.SetActive(true);
            Foco3.SetActive(true);
            Foco4.SetActive(true);
            Foco5.SetActive(true);
            Foco6.SetActive(true);
            Foco7.SetActive(true);
            Foco8.SetActive(true);



        }
        else
        {
            Foco1.SetActive(false);
            Foco2.SetActive(false);
            Foco3.SetActive(false);
            Foco4.SetActive(false);
            Foco5.SetActive(false);
            Foco6.SetActive(false);
            Foco7.SetActive(false);
            Foco8.SetActive(false);
        }


    }


    // Update is called once per frame
    void Update()
    {
        x = Input.GetAxis("Horizontal");
        y = Input.GetAxis("Vertical");

        transform.Rotate(0, x * Time.deltaTime * velocidadRotacion, 0);
        transform.Translate(0,0,y*Time.deltaTime*velocidadMovimiento);


        anim.SetFloat("VelX",x);
        anim.SetFloat("VelY",y);


        if (puedoSaltar)
        {
            if (Input.GetKeyDown(KeyCode.Space)) { 
           
                anim.SetBool("salte", true);
                rb.AddForce(new Vector3(0,fuerzaDeSalto,0),ForceMode.Impulse);
            }
            anim.SetBool("tocoSuelo", true);

        }
        else
        {
            EstoyCayendo();

        }
       

    }

    public void EstoyCayendo()
    {
        //caer rapido
        rb.AddForce(fuerzaExtra*Physics.gravity);
        //


        anim.SetBool("tocoSuelo", false);
        anim.SetBool("salte", false);

    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.name == "Profesora1")
        {
            
            Profesora1 = true;
            p1 = 1;
           
        }

        if (other.name == "Profesora2")
        {
            
            Profesora2 = true;
            p1 = 2;
           
        }
        if (other.name == "SwitchSimple")
        {

            SwitchSimple = true;
            p1 = 3;

        }

        if (other.name == "BombillaLed")
        {

            SwitchSimple = true;
            p1 = 4;

        }
        if (other.name == "LuminariaFluorescente4")
        {

            LuminariaFluorescente4 = true;
            p1 = 5;

        }
        if (other.name == "SwitchLuminariaFluorescente4")
        {

            SwitchLuminariaFluorescente4 = true;
            p1 = 6;

        }
        if (other.name == "LamparaCuadrada4")
        {

            LamparaCuadrada4 = true;
            p1 = 7;

        }
        if (other.name == "SwitchLamparaCuadrada4")
        {

            SwitchLamparaCuadrada4 = true;
            p1 = 8;

        }
    }


    public void OnTriggerExit(Collider other)
    {
        if (other.name == "Profesora1")
        {
           
            Profesora1 = false;
            
            
        }

        if (other.name == "Profesora1")
        {
           
            Profesora2 = false;
           
          
        }

        if (other.name == "SwitchSimple")
        {

            SwitchSimple = false;

           
        }

        if (other.name == "BombillaLed")
        {

            BombillaLed = false;

            
        }
        if (other.name == "LuminariaFluorescente4")
        {

            LuminariaFluorescente4 = false;


        }
        if (other.name == "SwitchLuminariaFluorescente4")
        {

            SwitchLuminariaFluorescente4 = false;


        }

        

 if (other.name == "LamparaCuadrada4")
        {

            LamparaCuadrada4 = false;


        }

        if (other.name == "SwitchLamparaCuadrada4")
        {

            SwitchLamparaCuadrada4 = false;


        }




    }
    //esperar3segundos
    IEnumerator Esperar()
    {
        yield return new WaitForSeconds (3);
        panelCambioCamara.SetActive(false);
    }

}
