﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchfocoLedEmpotrado : MonoBehaviour
{
    public LogicaPersonaje1 j;
    public int P1;
    public GameObject panelEncender;
    public GameObject Foco;
    public bool entro = false;
   
    // Start is called before the first frame update
    void Start()
    {
        
    Foco.gameObject.SetActive(false);
        
    }

    // Update is called once per frame
    void Update()
    {
        j = FindObjectOfType<LogicaPersonaje1>();

        P1 = j.p1;
    }
    public void OnTriggerEnter(Collider other)
    {
        panelEncender.SetActive(true);
        entro = true;
}
    public void OnTriggerExit(Collider other)
    {
        panelEncender.SetActive(false);
        entro = false;
    }


    public void No()
    {
        if (entro == true)
        {

            Foco.SetActive(false);
        }
    }


    public void Si()
    {
        if (entro==true) {
            Foco.SetActive(true);
        }
    }

    



}
