﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Control_puerta : MonoBehaviour
{
    Animator anim;
    public bool Dentro = false;
    bool puerta=false;
    public GameObject panelAbrirPuerta;
    
    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
      
    }

    private void OnTriggerEnter(Collider col)
    {
        if (col.tag == "Player")
        {
            Dentro = true;
            panelAbrirPuerta.SetActive(true);
        }
    }

    void OnTriggerExit(Collider col)
    {
        if (col.tag == "Player")
        {
            Dentro = false;
            panelAbrirPuerta.SetActive(false);
        }
    }


    // Update is called once per frame
    void Update()
    {
        if (Dentro)
        {
            puerta = !puerta;
        }
    }

    public void No()
    {
        
        if (Dentro == true)
        {

            anim.SetBool("abierta", true);
        }
        
    }


    public void Si()
    {
        
        if (Dentro == true )
        {
            anim.SetBool("abierta", false);
        
        }
    }






















}
