﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Calculadora : MonoBehaviour
{

    public GameObject panelFlujo;
    public GameObject panelFlujo1;
    public GameObject panelFlujoSiNo;
    public bool entro = false;

    public LogicaPersonaje1 j;
    public int P1;
    public LogicaPersonaje1 jugador;

    // Start is called before the first frame update
    void Start()
    {
        

    }

    // Update is called once per frame
    void Update()
    {
        j = FindObjectOfType<LogicaPersonaje1>();

        P1 = j.p1;
      
    }


    public void OnTriggerEnter(Collider other)
    {
        panelFlujoSiNo.SetActive(true);
        entro = true;
    }
    public void OnTriggerExit(Collider other)
    {
        panelFlujoSiNo.SetActive(false);
        entro = false;
    }

    public void ButtonSalirGeneralCatalogo()
    {
        if (entro == true)
        {
            panelFlujo.SetActive(false);
            panelFlujo1.SetActive(false);
            jugador.enabled = true;
        }
    }


    public void No()
    {
        if (entro == true)
        {

            panelFlujo.SetActive(false);
            panelFlujo1.SetActive(false);

        }
    }


    public void Si()
    {
        if (entro == true)
        {
            panelFlujo.SetActive(true);
            panelFlujo1.SetActive(true);
            panelFlujoSiNo.SetActive(false);
        }
    }
}
